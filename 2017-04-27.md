# April 27, 2017 - GoDaddy Dispute

## What happened
Two years ago, we decided to regsiter the `pitter.us` domain with this regisar, in the intents to kickstart this service.  
While our domain renews at 2017-05-31, we wanted to try to go ahead and transfer our domain and renew it under Google Domains, for the sole purpose of it being cost effective and straightforward to manage.  
During this process, the previous registar has decided to decline the request, and deliberately block any form of attempt to transfer, with a 60-day lock on the domain.  

## Why not dispute the lock?
From the date of this incident, it would require more time under lock, than if we simply let the domain expire.  
When we originally bought the domain under the `.us` TLD, we honestly felt uncomfortable with the domain being region specific but went with it simply for the name.

## New plans...
As of today, we've bought the domain `pitterapp.com` with Google and will slowly migrate the new backend in testing to it.